import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { HeroesService, Heroe} from '../../services/heroes.service'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {

  filterHeroes:Heroe[] = []
  msg:string
  termino:string

  constructor(public activatedRoute:ActivatedRoute, public _filterHeroes:HeroesService, public router:Router) {

  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.filterHeroes = this._filterHeroes.buscarHeroes(params['heroesFilter'])
      this.termino = params['heroesFilter']
    })

  }

  verHeroe(idx:number){
    this.router.navigate(['/heroe', idx])
  }

}
